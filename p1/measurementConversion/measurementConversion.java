import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class measurementConversion
{
	public static void main(String[] args) {

        int inches = 0;
        final double INCHES_TO_CENTIMETER = 2.54;
        final double INCHES_TO_METER = 0.0254;
        final double INCHES_TO_FOOT = 0.083333;
        final double INCHES_TO_YARD = 0.0277778;
        final double INCHES_TO_MILE = 0.00001578;
        boolean done = false;
        Scanner scnr = new Scanner(System.in);


        
        

        do {
            

            try {
                System.out.print("Please enter number of inches: ");
                inches = scnr.nextInt();
                System.out.println(inches + " inch(es) equals\n");
                System.out.printf("%.6f centimeter(s)\n", inches * INCHES_TO_CENTIMETER);
                System.out.printf("%.6f meter(s)\n", inches * INCHES_TO_METER);
                System.out.printf("%.6f feet\n", inches * INCHES_TO_FOOT);
                System.out.printf("%.6f yard(s)\n", inches * INCHES_TO_YARD);

                System.out.printf("%.8f mile(s)\n\n", inches * INCHES_TO_MILE);
                break;
            }

            catch (InputMismatchException e) {
                System.out.println("Not a valid integer!\n");
                scnr.next();
            }

        }while (done == false);
    }
}