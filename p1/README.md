# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Project 1 Requirements:

	1. Create app that allows user to play 3 different songs
	2. Ensure that only 1 song can be played at a time
	3. Include artwork
	4. Custom splash screen


#### Assignment Screenshots:

| *Splash Screen*: | *Opening Screen* |
| :---                  |                  ---: |
| ![Splash](img/splash.png) | ![Opening](img/static.png) |


| *Playing Screen* : | *Pause Screen* |
| :---                  |                  ---: |
| ![Toast](img/playing.png) | ![Running](img/pause.png) |


*Measurement Conversion Skillset*  
![Measurement Conversion](img/ss7.png)

*Distance Calculator Skillset*  
![Distance Calculator](img/ss8.png)

*Multiple Selection List Skillset*  
![Distance Calculator](img/ss9.gif)
