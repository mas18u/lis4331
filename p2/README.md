# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Project 2 Requirements:

	1. Create app that uses SQLite database (persistant data)  
	2. Insert 5 users  
	3. Implement ability to add, update, and delete users from database  


#### Assignment Screenshots:

| *Splash Screen*: | *Add User* |
| :---                  |                  ---: |
| ![Splash](img/splash.png) | ![Add](img/add.png) |


| *Update User* : | *View Users* |
| :---                  |                  ---: |
| ![Update](img/update.png) | ![View](img/view.png) |

| *Delete User* : | |
| :---                  |                  ---: |
| ![Delete](img/delete.png) |  |

