import java.io.*;

public class writeRead
{
public static void main(String [] args)
{

String myFile = "filewrite.txt";

try {
    //get IVM system encoding (default character set
    // CP1252: windows 1252 codepage, also called Latin 1
    //System.out.printla(System.getrinnertx("file.encoding"));
    //assume default encoding
    FileWriter fileWriter = new FileWriter(myFile);
    //Note: Without buffering, each method invocation of classes ElleReades/ElleWaiter
    //bytes read/write from file, convert into characters, then returned--very inefficient.
    //instead: wrap File Writer in Buffered Writec
    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    //write() does not append newline character
    bufferedWriter.write("Fourscore and seven years ago our fathers brought forth, ");
    bufferedWriter.newLine();
    bufferedWriter.write("on this continent, a new nation, conceived in liberty, ");
    bufferedWriter.newLine();
    bufferedWriter.write("and dedicated to the proposition that all men are created equal.");
    //close file
    bufferedWriter.close();
}
    catch(IOException ex)
        {
        System.out.println("Error writing to file " + myFile + "'");
        //Or...print error
        // ex. QuintStackTrace();
        }
    }
}
