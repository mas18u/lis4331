# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Assignment #5 Requirements:

1. Create a news app that uses an RSS feed     
2. Write/Read Java Skillset 13  
3. Simple Interest Calculator Java Skillset 14  
4. Array Demo Using Methods Java Skillset 15  

#### Assignment Screenshots:

| *Items Activity*: | *Item Activity* |
| :---                  |                  ---: |
| ![Items Activity](img/itemsActivity.png) | ![Item](img/itemActivity.png) |


| *Read More* : |  |
| :---                  |                  ---: |
| ![Read More](img/readMore.png) |  |


*Wite/Read Java Skillset 13*  
![SS13](img/ss13.png)

*Simple Interest Calculator Skillset 14*  
![SS14](img/ss14.png)

*Array Demo Using Methods Skillset 15*  
![SS15](img/ss15.png)
