import java.util.InputMismatchException;
import java.util.Scanner;

public class travelTime
{
    public static void main(String[] args)
    {
        double miles =0.0;
        double mph = 0.0;
        boolean done = false;
        double travelTime;
        Scanner scnr = new Scanner(System.in);
        String response = "y";

do{
        System.out.println("Program calculates approximate travel time\n\n");
        System.out.print("Please enter miles: ");
        

    do {
        
        try {    
           miles = scnr.nextDouble(); 
           while((miles < 0) || (miles > 3000)) {
                System.out.println("Miles must be greater than 0, and no more than 3000.\n");
                System.out.print("Please enter miles: ");
                miles = scnr.nextDouble();
            }
            done = true;
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid double--miles must be a number.\n");
            System.out.print("Please enter miles: ");
            scnr.next();
        }
    }while(done == false);

    done = false;
    System.out.print("Please enter MPH: ");

    do {
        try {    
            mph = scnr.nextDouble();

           while((mph < 0) || (mph > 100)) {
                System.out.println("MPH must be greater than 0, and no more than 100.\n");
                System.out.print("Please enter MPH: \n");
                mph = scnr.nextDouble();
            }
            done = true;
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid double--MPH must be a number.\n");
            System.out.print("Please enter MPH: \n");
            scnr.next();
        }
    }while(done == false);
    
    travelTime = (miles/mph);
    double hours = Math.floor(travelTime);
    double minutes = travelTime % 60;

    System.out.print("Estimated travel time: ");
    System.out.printf("%.0f", + hours);
    System.out.print(" hr(s) ");
    System.out.printf("%.0f", + Math.floor(((minutes - hours) * 60)));
    System.out.print(" Minutes\n\n");

    System.out.print("Continue? (y/n): ");
    response = scnr.next();

}while(response.equalsIgnoreCase("y"));
}
}