# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Assignment #4 Requirements:

1. Create app that calculates interest paid for a mortgage  
2. App uses persistant data  
3. Travel Time Java Skillset 10  
4. Product Class Java Skillset 11  
5. Book Inherits Product Class Java Skillset 12  

#### Assignment Screenshots:

| *Splash Screen*: | *Main* |
| :---                  |                  ---: |
| ![Splash](img/splash.png) | ![Main](img/main.png) |


| *Incorrect Number of Years* : | *Valid* |
| :---                  |                  ---: |
| ![Incorrect](img/incorrect.png) | ![Valid](img/valid.png) |


*Travel Time Skillset 10*  
![Travel Time](img/travel.png)

*Product Class Skillset 11*  
![Product Class](img/product.png)

*Java Book Inherits Skillset 12 #1*  
![SS12_1](img/ss12_1.png)

*Java Book Inherits Skillset 12 #2*  
![SS12_2](img/ss12_2.png)
