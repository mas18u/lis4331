import java.util.Scanner;

class bookDemo
{
    public static void main(String[] args)
    {
        String cd = "";
        String des = "";
        double pr = 0.00;
        String au = "";
        Scanner scnr = new Scanner(System.in);

        System.out.println("\n/////Below are *base class dafault constructor* values (instantiating p1, then using getter methods)://///\n");
        product p1 = new product();
        System.out.println("\nCode = " + p1.getCode());
        System.out.println("\nDescription = " + p1.getDescription());
        System.out.println("\nPrice = $" + p1.getPrice());

        System.out.println("\n/////Below are *base class* user-entered values (instantiating p2, then using getter methods)://///");

        System.out.print("\nCode: ");
        cd = scnr.nextLine();

        System.out.print("\nDescription: ");
        des = scnr.nextLine();

        System.out.print("\nPrice: $");
        pr = scnr.nextDouble();

        product p2 = new product(cd, des, pr);
        System.out.println("\nCode = " + p2.getCode());
        System.out.println("\nDescription = " + p2.getDescription());
        System.out.println("\nPrice = $" + p2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values to p2, then print() method to display values://///");
        p2.setCode("xyz789");
        p2.setDescription("Test Widget");
        p2.setPrice(89.99);
        p2.print();

        System.out.println();

        System.out.println("\n/////Below are *derived class default constructor* values (instantiating b1, then using getter methods)://///");
        book b1 = new book();
        System.out.println("\nCode = " + b1.getCode());
        System.out.println("\nDescription = " + b1.getDescription());
        System.out.println("\nPrice = $" + b1.getPrice());
        System.out.println("\nAuthor = " + b1.getAuthor());

        System.out.println("\nOr using overridden derived class print() method...");
        b1.print();

        System.out.println("\n/////Below are *derived class* user-entered values (instantiating b2, then using getter methods)://///");

        System.out.print("\nCode: ");
        cd = scnr.next();

        System.out.print("\nDescription: ");
        des = scnr.next();

        System.out.print("\nPrice: ");
        pr = scnr.nextDouble();

        System.out.print("\nAuthor: ");
        au = scnr.next();

        book b2 = new book(cd, des, pr, au);
        System.out.println("\nCode = " + b2.getCode());
        System.out.println("\nDescription = " + b2.getDescription());
        System.out.println("\nPrice = $" + b2.getPrice());
        System.out.println("\nAuthor = " + b2.getAuthor());

        System.out.println("\nOr using derived class print() method...");
        b2.print();

    }
}