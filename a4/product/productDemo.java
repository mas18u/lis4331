import java.util.Scanner;

class productDemo
{
    public static void main(String[]args)
    {

        String cd = "";
        String des = "";
        double price = 0.00;
        Scanner scnr = new Scanner(System.in);

        System.out.println("\n/////Below are default constructer values://///\n");
        product p1 = new product();
        System.out.println("\nCode = " + p1.getCode());
        System.out.println("\nDescription = " + p1.getDescription());
        System.out.println("\nPrice = $" + p1.getPrice());

        System.out.println("\n/////Below are user-entered values://///");

        System.out.print("\nCode: ");
        cd = scnr.nextLine();

        System.out.print("\nDescription: ");
        des = scnr.nextLine();

        System.out.print("\nPrice: $");
        price = scnr.nextDouble();

        product p2 = new product(cd, des, price);
        System.out.println("\nCode = " + p2.getCode());
        System.out.println("\nDescription = " + p2.getDescription());
        System.out.println("\nPrice = $" + p2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        p2.setCode("xyz789");
        p2.setDescription("Test Widget");
        p2.setPrice(89.99);
        p2.print();
    }
}