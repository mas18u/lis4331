class product
{
    private String code;
    private String description;
    private double price;


public product()
{
    System.out.println("\nInside Product default constructor");
    code = "abc123";
    description = "My Widget";
    price = 49.99;
}

public product(String code, String description, double price)
{
    System.out.println("\nInside product constructer with parameters");

    this.code = code;
    this.description = description;
    this.price = price;
}

public String getCode()
{
    return code;
}

public void setCode(String cd)
{
    code = cd;
}

public String getDescription()
{
    return description;
}

public void setDescription(String des)
{
    description = des;
}

public double getPrice()
{
    return price;
}

public void setPrice(double pr)
{
   price = pr;
}

public void print()
{
    System.out.println("\nCode: " + code + "\nDescription: " + description + "\nPrice: $" + price);

}
}