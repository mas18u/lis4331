class book extends product{

    private String author;

public book()
{
    super();
    System.out.println("\nInside book default constructer");

    author = "John Doe";

}

public book(String cd, String des, double pr, String au)
{
    super(cd, des, pr);
    System.out.println("\nInside book constructer with parameters");

    author = au;
}

public String getAuthor()
{
    return author;
}

public void setAuthor(String au)
{
    author = au; 
}

public void print()
{
    super.print();
    System.out.println("Author: " + author);
}

}