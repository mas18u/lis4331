# LIS 4331 Advanced Mobile App Development

## Manolo Sanchez

### Assignment #1 Requirements:

*Four Parts*

1. Distributed version control with git and bitbucket
2. development installations
3. Chapter questions (1 and 2)
4. bitbucket repo links a) This assignment b) the completed tutorials above (bitbucket station locations and myteamquotes)

#### README.md file should include the following items:

* screenshot of running java hello
* screenshot of running android studio - my first app
* screenshots of running android studio - contacts app
* git commands w/ short descriptions

> #### Git commands w/short descriptions:

1. git init - creates an empty git repository
2. git status - shows the working tree status
3. git add - adds file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git clone - downloads existing source code from a remote repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![java Hello](img/Hello.png)

*Screenshot of Android Studio - My First App*:

![My First App](img/MyFirstApp.png)

*Screenshot of Contacts App - Main Screen*:

![Android Studio Installation Screenshot](img/Contacts_Main.png)

*Screenshot of Contacts App - Info Screen*:
![Android Studio Installation Screenshot](img/Contacts_Example.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mas18u/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
