# LIS 4331 Advanced Mobile App Development

## Manolo Sanchez

### LIS 4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")  
		- Install JDK  
		- Install Android Studio and create My First App and Contacts App  
		- Provide screenshots of installations  
		- Create Bitbucket repo  
		- Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)  
		- Provide git command descriptions  

2. [A2 README.md](a2/README.md "My A2 README.md file")  
		- Create Tip Calculator App with Custom Theme  
		- Drop-down menu for total number of guests (including yourself)   
		- Drop-down menu for tip percentage (5% increments)   
		- Must add background color(s) or theme   
		- Must create and display launcher icon image  
		- Java skill sets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")  
		- Create app that converts US Dollars to Various currencies    
		- Screenshot of splash screen  
		- Screenshot of unpopulated UI  
		- Screenshot of toast notification  
		- Screenshot of running converted currency UI  
		- Java Skill sets 4-6  
		
4. [P1 README.md](p1/README.md "My P! README.md file")  
		- Create app that allows user to play 3 different songs  
		- Ensure that only 1 song can be played at a time  
		- Include artwork  
		- Custom splash screen  

5. [A4 README.md](a4/README.md "My A4 README.md file")  
		- Create app that calculates interest paid for a mortgage  
		- App uses persistant data  
		- Travel Time Java Skillset 10  
		- Product Class Java Skillset 11  
		- Book Inherits Product Class Java Skillset 12  

6. [A5 README.md](a5/README.md "My A5 README.md file")  
		- Create a news app that uses an RSS feed     
		- Write/Read Java Skillset 13  
		- Simple Interest Calculator Java Skillset 14  
		- Array Demo Using Methods Java Skillset 15  

7. [A2 README.md](p2/README.md "My A2 README.md file")  
		- Create app that uses SQLite database (persistant data)  
		- Insert 5 users  
		- Implement ability to add, update, and delete users from database  

