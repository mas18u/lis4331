import java.util.InputMismatchException;
import java.util.Scanner;

public class circle
{
	public static void main(String[] args) {
		double radius;
		boolean done = false;
		Scanner scnr = new Scanner(System.in);

		System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
		System.out.println("Must use Java's built-in constant, printf(), and formatted to 2 decimal places.");
		System.out.println("Must *only* permit numeric entry.\n");

		do
		{
			try
			{
				System.out.print("Enter circle radius: ");
				radius = scnr.nextInt();
				System.out.printf("\nCircle Diameter: %.2f\nCircumference: " + "%.2f\nArea: %.2f\n",
				 (2 * radius), (2*Math.PI*radius), (Math.PI*radius*radius));
				done = true;
				break;
		}
			catch (InputMismatchException e)
			{
				System.out.println("Not a valid number!");
				System.out.println();
				System.out.print("Please try again. ");
				scnr.next();
			}
		} while (done == false);
		

	}
}