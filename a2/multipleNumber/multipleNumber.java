import java.util.InputMismatchException;
import java.util.Scanner;

public class multipleNumber
{
	public static void main(String[] args) {
        int firstNumber;
        int secondNumber;
        boolean done = false;
        Scanner scnr = new Scanner(System.in);

        System.out.println("Program determines if first number is multiple of second, prints result.");
        System.out.println("Example: 2, 4, 6, 8 are multiples of 2.");
        System.out.println("1) Use integers. 2) Use printf() function to print.");
        System.out.println("Must *only* permit integer entry.\n");

        do {

            try {
                System.out.print("Num1: ");
                firstNumber = scnr.nextInt();
                System.out.print("Num2: ");
                secondNumber = scnr.nextInt();
                System.out.println();

                if ((firstNumber % secondNumber) == 0) {
                    System.out.println(firstNumber + " is a multiple of " + secondNumber);
                    System.out.println("The product of " + (firstNumber/secondNumber) + " and " + secondNumber + " is " + firstNumber);
                    break;
                }
            }

            catch (InputMismatchException e) {
                System.out.println("Not a valid integer!");
                System.out.println();
                System.out.print("Please try again. Enter ");
                scnr.next();

            }

        }while (done == false);
        
        
    }     
}