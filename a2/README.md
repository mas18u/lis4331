# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Assignment #2 Requirements:

1. Drop-down menu for total number of guests (including yourself): 1 – 10  
2. Drop-down menu for tip percentage (5% increments): 0 – 25  
3. Must add background color(s) or theme (10 pts)  
4. Must create and display launcher icon image (10 pts)  
5. Java skill sets 1-3



#### Assignment Screenshots:

| *Tip Calculator App*: | *Gif of App Working*: |
| :---                  |                  ---: |
| ![App Running](img/app_working.png) | ![JDK Installation Screenshot](img/tipCalc.gif) |


*Java Circle Measurements*
![Circle](img/circle.png)

*Java Multiple Number and Factor Calculator*
![Multiple](img/multipleNumber.png)

*Java Nested Structure Grade Average Calculator*
![nestedStructures](img/nestedStructures.png)