# LIS 4331 - Adv Mobile App Development

## Manolo Sanchez

### Assignment #3 Requirements:

1. Create app that converts US Dollars to various currencies  
2. Screenshot of splash screen
3. Screenshot of unpopulated UI
4. Screenshot of toast notification
5. Screenshot of running converted currency UI
6. Time Conversion Java skillset (5)
7. Even or Odd (GUI) skillset (6)
8. Paint Calculator (GUI) skillset (7)


#### Assignment Screenshots:

| *Splash Screen*: | *Unpopulated UI* |
| :---                  |                  ---: |
| ![Splash](img/splash.png) | ![Unpopulated](img/unpopulated.png) |


| *Toast* : | *Running App* |
| :---                  |                  ---: |
| ![Toast](img/toast.png) | ![Running](img/running.png) |


*Time Conversion Skillset*  
![Time Conversion](img/timeConversion.png)

*Even or Odd (GUI) Skillset*  
![SS5](img/ss5.gif)

*Paint Calculator(GUI) Skillset*  
![SS6](img/ss6.gif)
